// jshint ignore: start
const express = require('express')
const app = express()
var path = require('path');

var publicPath = path.join(__dirname + '/public');

app.use(express.static(publicPath))

app.listen(3000, () => console.log('Your server available at http://localhost:3000/'))